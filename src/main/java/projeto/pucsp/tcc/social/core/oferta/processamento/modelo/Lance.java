package projeto.pucsp.tcc.social.core.oferta.processamento.modelo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "lance")
@Data
public class Lance {

    private static final Integer LANCE_COMPENSANDO_POR_COMPLETO = 2;

    private static final Integer LANCE_COMPENSANDO_PARCIALMENTE = 1;

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "social_id")
    private Integer socialId;

    @Column(name = "alimento_id")
    private Integer alimentoId;

    @Column(name = "situacao_lance")
    private Integer situacaoLance;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    @Transient
    private Integer quantidadeAlimentoCompensado;

    public void compensar(Integer quantidadeAlimento) {

        if (calcularVariacaoCompensamento(quantidadeAlimento) >= 0) {

            quantidadeAlimentoCompensado = compensar();

        } else {

            this.quantidadeAlimento -= quantidadeAlimento;

            situacaoLance = LANCE_COMPENSANDO_PARCIALMENTE;

            quantidadeAlimentoCompensado = quantidadeAlimento;

        }

    }

    private Integer compensar() {

        final Integer quantidadeAlimentoAnterior = this.quantidadeAlimento;

        situacaoLance = LANCE_COMPENSANDO_POR_COMPLETO;

        this.quantidadeAlimento = 0;

        return quantidadeAlimentoAnterior;

    }

    private Integer calcularVariacaoCompensamento(Integer quantidadeAlimento) {
        return quantidadeAlimento - this.quantidadeAlimento;
    }

}
