package projeto.pucsp.tcc.social.core.oferta.processamento.modelo;

import lombok.Data;

@Data
public class OfertaPublicacao {

    private Integer ofertaId;

    private Integer empresarialId;

    private Integer alimentoId;

    private Integer quantidadeAlimento;

    private Localizacao localizacao;

}
