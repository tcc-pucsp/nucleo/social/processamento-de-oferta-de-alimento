package projeto.pucsp.tcc.social.core.oferta.processamento.cliente;

import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaCompensada;

public interface ClienteConcluirOferta {

    void publicar(OfertaCompensada ofertaCompensada);

}
