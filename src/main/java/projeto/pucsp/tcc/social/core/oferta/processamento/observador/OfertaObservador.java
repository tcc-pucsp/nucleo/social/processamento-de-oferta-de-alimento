package projeto.pucsp.tcc.social.core.oferta.processamento.observador;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.OfertaRecurso;

@Slf4j
@Component
public class OfertaObservador implements OfertaRecurso {

    private final OfertaRecurso ofertaRecurso;

    public OfertaObservador(@Qualifier("ofertaServico") OfertaRecurso ofertaRecurso) {
        this.ofertaRecurso = ofertaRecurso;
    }

    @RabbitListener(queues = "processar.oferta")
    @Override
    public void processar(OfertaPublicacao ofertaPublicacao) {

        log.info("Processar oferta  : {}", ofertaPublicacao);

        ofertaRecurso.processar(ofertaPublicacao);

    }
}
