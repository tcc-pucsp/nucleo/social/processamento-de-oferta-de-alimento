package projeto.pucsp.tcc.social.core.oferta.processamento.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rabbit.compensar.oferta")
public class PropriedadeConcluirOferta {

    private String topicExchange;

}
