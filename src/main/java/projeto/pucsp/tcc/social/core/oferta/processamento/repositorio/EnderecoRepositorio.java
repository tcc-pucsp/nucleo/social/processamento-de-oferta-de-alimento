package projeto.pucsp.tcc.social.core.oferta.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Endereco;

public interface EnderecoRepositorio extends JpaRepository<Endereco, Integer> {
}
