package projeto.pucsp.tcc.social.core.oferta.processamento.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.oferta.processamento.cliente.ClienteConcluirOferta;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Lance;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaCompensada;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.OfertaRecurso;

import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class OfertaServico implements OfertaRecurso {

    private final LanceRecurso lanceRecurso;

    private final ClienteConcluirOferta clienteConcluirOferta;

    public OfertaServico(LanceRecurso lanceRecurso, ClienteConcluirOferta clienteConcluirOferta) {
        this.lanceRecurso = lanceRecurso;
        this.clienteConcluirOferta = clienteConcluirOferta;
    }

    @Override
    public void processar(OfertaPublicacao ofertaPublicacao) {

        log.info("Processando em base de dados lances Disponiveis para oferta com id : {}", ofertaPublicacao.getOfertaId());

        final List<Lance> lances = lanceRecurso
                .obterLances(
                        ofertaPublicacao.getAlimentoId(),
                        ofertaPublicacao.getLocalizacao());

        if (lances.isEmpty()) {

            log.info("Não existe lance para compensar a oferta {}", ofertaPublicacao);

            return;

        }

        compensarLancesDisponiveis(ofertaPublicacao, lances);

    }

    private void compensarLancesDisponiveis(OfertaPublicacao ofertaPublicacao, List<Lance> lances) {

        final Iterator<Lance> iterator = lances.iterator();

        Integer quantidadeAlimento = ofertaPublicacao
                .getQuantidadeAlimento();

        while (quantidadeAlimento > 0 && iterator.hasNext()) {

            final Lance lance = iterator.next();

            log.info("{}", lance);

            lance.compensar(quantidadeAlimento);

            final boolean atualizado = lanceRecurso
                    .atualizar(lance);

            if (atualizado) {

                log.info("{} compensado com sucesso", lance);

                publicarLanceCompensado(ofertaPublicacao, lance);

                quantidadeAlimento -= lance.getQuantidadeAlimentoCompensado();

            }
        }
    }

    private void publicarLanceCompensado(OfertaPublicacao ofertaPublicacao, Lance lance) {

        clienteConcluirOferta
                .publicar(new OfertaCompensada(ofertaPublicacao, lance));

    }

}

