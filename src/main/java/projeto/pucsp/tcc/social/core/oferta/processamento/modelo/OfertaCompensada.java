package projeto.pucsp.tcc.social.core.oferta.processamento.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class OfertaCompensada {

    private final Integer ofertaId;

    private final Integer lanceId;

    private final Integer empresarialId;

    private final Integer socialId;

    private final Integer alimentoId;

    private final Integer quantidadeAlimento;

    public OfertaCompensada(OfertaPublicacao ofertaPublicacao, Lance lance) {

        ofertaId = ofertaPublicacao.getOfertaId();

        lanceId = lance.getId();

        empresarialId = ofertaPublicacao.getEmpresarialId();

        socialId = lance.getSocialId();

        alimentoId = ofertaPublicacao.getAlimentoId();

        quantidadeAlimento = lance.getQuantidadeAlimentoCompensado();

    }
}
