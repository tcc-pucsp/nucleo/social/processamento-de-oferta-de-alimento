package projeto.pucsp.tcc.social.core.oferta.processamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.social.core.oferta.processamento.propriedade.PropriedadeConcluirOferta;

@SpringBootApplication
@EnableConfigurationProperties(PropriedadeConcluirOferta.class)
public class ProcessamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcessamentoApplication.class, args);
	}

}
