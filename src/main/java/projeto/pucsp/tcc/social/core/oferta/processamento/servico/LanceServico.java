package projeto.pucsp.tcc.social.core.oferta.processamento.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Lance;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Localizacao;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.oferta.processamento.repositorio.LanceRepositorio;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public
class LanceServico implements LanceRecurso {

    private final LanceRepositorio repositorio;

    public LanceServico(LanceRepositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Lance> obterLances(Integer alimentoId, Localizacao localizacao) {

        return repositorio.obterLances(
                alimentoId,
                new BigDecimal(localizacao.getLatitude()),
                new BigDecimal(localizacao.getLongitude()));
    }

    @Override
    public Boolean atualizar(Lance lance) {

        log.info("Compensando lance : {} ", lance);

        return
                repositorio.atualizarLance(lance.getId(), lance.getSituacaoLance(), lance.getQuantidadeAlimentoCompensado()).equals(1);

    }

}
