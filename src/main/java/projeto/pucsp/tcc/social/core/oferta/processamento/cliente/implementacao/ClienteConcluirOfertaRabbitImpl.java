package projeto.pucsp.tcc.social.core.oferta.processamento.cliente.implementacao;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.oferta.processamento.cliente.ClienteConcluirOferta;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaCompensada;
import projeto.pucsp.tcc.social.core.oferta.processamento.propriedade.PropriedadeConcluirOferta;

@Component
public class ClienteConcluirOfertaRabbitImpl implements ClienteConcluirOferta {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeConcluirOferta propriedadeConcluirOferta;

    public ClienteConcluirOfertaRabbitImpl(RabbitTemplate rabbitTemplate, PropriedadeConcluirOferta propriedadeConcluirOferta) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeConcluirOferta = propriedadeConcluirOferta;
    }

    @Override
    public void publicar(OfertaCompensada ofertaCompensada) {

        rabbitTemplate.convertAndSend(propriedadeConcluirOferta.getTopicExchange(), ofertaCompensada);

    }
}
