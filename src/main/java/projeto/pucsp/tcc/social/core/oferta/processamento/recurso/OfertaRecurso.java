package projeto.pucsp.tcc.social.core.oferta.processamento.recurso;

import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaPublicacao;

public interface OfertaRecurso {

    void processar(OfertaPublicacao ofertaPublicacao);

}
