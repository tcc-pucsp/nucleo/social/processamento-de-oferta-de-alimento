package projeto.pucsp.tcc.social.core.oferta.processamento.recurso;

import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Lance;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Localizacao;

import java.util.List;

public interface LanceRecurso {

    List<Lance> obterLances(Integer alimentoId, Localizacao localizacao);

    Boolean atualizar(Lance lance);

}
