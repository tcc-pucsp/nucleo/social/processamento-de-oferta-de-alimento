package projeto.pucsp.tcc.social.core.oferta.processamento.modelo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "social")
@Data
public class Social {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "endereco_fk")
    private Integer enderecoFk;

}
