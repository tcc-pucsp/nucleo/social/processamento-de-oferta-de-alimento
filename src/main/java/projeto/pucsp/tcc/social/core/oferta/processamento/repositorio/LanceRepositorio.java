package projeto.pucsp.tcc.social.core.oferta.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Lance;

import java.math.BigDecimal;
import java.util.List;

public interface LanceRepositorio extends JpaRepository<Lance, Integer> {


    /**
     * @param alimentoId identificador de alimento
     * @param latitude   latitude em BigDecimal
     * @param longitude  longitutde em BigDecimal
     * @return lista de Lance Disponivel para uma distancia menor que 10km
     * @apiNote A fórmula de Haversine é uma importante equação usada em navegação,
     * fornecendo distâncias entre dois pontos de uma esfera a partir de suas latitudes e longitudes.
     * É um caso especial de uma fórmula mais geral de trigonometria esférica, a lei dos Haversines,
     * relacionando os lados a ângulos de uma esfera "triangular".
     * https://pt.wikipedia.org/wiki/F%C3%B3rmula_de_Haversine
     */
    @Query(value = "select l.*,\n" +
            "       (6371 *\n" +
            "        acos(\n" +
            "                        cos(radians(:latitude)) *\n" +
            "                        cos(radians(e.latitude)) *\n" +
            "                        cos(radians(:longitude) - radians(e.longitude)) +\n" +
            "                        sin(radians(:latitude)) *\n" +
            "                        sin(radians(e.latitude))\n" +
            "            )) as distancia\n" +
            "from lance l\n" +
            "         inner join social s on l.social_id = s.id\n" +
            "         inner join endereco e on s.endereco_fk = e.id\n" +
            "where l.alimento_id = :alimento_id and l.situacao_lance <> 2\n" +
            "group by l.id \n" +
            "having ABS(distancia) <= 10\n", nativeQuery = true)
    List<Lance> obterLances(
            @Param("alimento_id") Integer alimentoId,
            @Param("latitude") BigDecimal latitude,
            @Param("longitude") BigDecimal longitude);

    @Modifying
    @Transactional
    @Query("update Lance set situacaoLance = :situacaoLance, quantidadeAlimento = quantidadeAlimento - :quantidadeAlimento " +
            "where situacaoLance <> 2 and " +
            "id = :id and " +
            "(quantidadeAlimento - :quantidadeAlimento) >= 0")
    Integer atualizarLance(@Param("id") Integer id, @Param("situacaoLance") Integer situacaoLance, @Param("quantidadeAlimento") Integer quantidadeAlimento);

}
