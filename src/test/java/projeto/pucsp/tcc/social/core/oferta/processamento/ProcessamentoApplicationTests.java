package projeto.pucsp.tcc.social.core.oferta.processamento;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import projeto.pucsp.tcc.social.core.oferta.processamento.cliente.ClienteConcluirOferta;
import projeto.pucsp.tcc.social.core.oferta.processamento.cliente.implementacao.ClienteConcluirOfertaRabbitImpl;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Lance;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.Localizacao;
import projeto.pucsp.tcc.social.core.oferta.processamento.modelo.OfertaPublicacao;
import projeto.pucsp.tcc.social.core.oferta.processamento.observador.OfertaObservador;
import projeto.pucsp.tcc.social.core.oferta.processamento.propriedade.PropriedadeConcluirOferta;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.oferta.processamento.recurso.OfertaRecurso;
import projeto.pucsp.tcc.social.core.oferta.processamento.repositorio.LanceRepositorio;
import projeto.pucsp.tcc.social.core.oferta.processamento.servico.LanceServico;
import projeto.pucsp.tcc.social.core.oferta.processamento.servico.OfertaServico;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProcessamentoApplicationTests {

    private static final String LATITUDE = "-45.23424432";
    private static final String LONGITUDE = "-23.4364778";
    private static final String REPOSITORIO_DEVE_RETORNAR_NADA = "repositorio deve retornar nada";

    @Autowired
    private OfertaObservador ofertaObservador;

    @Autowired
    private LanceRepositorio repositorio;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private PropriedadeConcluirOferta propriedadeConcluirOferta;

    @Mock
    private LanceRepositorio repositorioMock;

    private OfertaObservador ofertaObservadorMock;

    @Before
    public void setup() {

        LanceRecurso lanceRecurso = new LanceServico(repositorioMock);

        ClienteConcluirOferta clienteCompensadorOferta = new ClienteConcluirOfertaRabbitImpl(rabbitTemplate, propriedadeConcluirOferta);

        final OfertaRecurso ofertaServico = new OfertaServico(lanceRecurso, clienteCompensadorOferta);

        ofertaObservadorMock = new OfertaObservador(ofertaServico);

    }

    @Test
    public void processarOfertaComLanceDisponivel() {

        final OfertaPublicacao ofertaPublicacao = new OfertaPublicacao();

        ofertaPublicacao.setAlimentoId(2);
        ofertaPublicacao.setEmpresarialId(1);
        ofertaPublicacao.setOfertaId(4);
        ofertaPublicacao.setQuantidadeAlimento(4);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        ofertaPublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterLances(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterLanceProxy(ofertaPublicacao, 1)));

        Mockito
                .when(repositorioMock.atualizarLance(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        ofertaObservadorMock.processar(ofertaPublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComLanceDisponivelSemPossibilidadeDeAtualizar() {

        final OfertaPublicacao ofertaPublicacao = new OfertaPublicacao();

        ofertaPublicacao.setAlimentoId(2);
        ofertaPublicacao.setEmpresarialId(1);
        ofertaPublicacao.setOfertaId(4);
        ofertaPublicacao.setQuantidadeAlimento(4);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        ofertaPublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterLances(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterLanceProxy(ofertaPublicacao, 1)));

        Mockito
                .when(repositorioMock.atualizarLance(anyInt(), anyInt(), anyInt()))
                .thenReturn(0);

        ofertaObservadorMock.processar(ofertaPublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComMaisLancesDoQueOferta() {

        final OfertaPublicacao ofertaPublicacao = new OfertaPublicacao();

        ofertaPublicacao.setAlimentoId(2);
        ofertaPublicacao.setEmpresarialId(1);
        ofertaPublicacao.setOfertaId(4);
        ofertaPublicacao.setQuantidadeAlimento(100);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        ofertaPublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterLances(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterLanceProxy(ofertaPublicacao, 1), obterLanceProxy(ofertaPublicacao, 2)));

        Mockito
                .when(repositorioMock.atualizarLance(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        ofertaPublicacao.setQuantidadeAlimento(1);

        ofertaObservadorMock.processar(ofertaPublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComMenosLancesDoQueOfertaNecessita() {

        final OfertaPublicacao ofertaPublicacao = new OfertaPublicacao();

        ofertaPublicacao.setAlimentoId(2);
        ofertaPublicacao.setEmpresarialId(1);
        ofertaPublicacao.setOfertaId(4);
        ofertaPublicacao.setQuantidadeAlimento(1);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        ofertaPublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterLances(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterLanceProxy(ofertaPublicacao, 1)));

        Mockito
                .when(repositorioMock.atualizarLance(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        ofertaPublicacao.setQuantidadeAlimento(100);

        ofertaObservadorMock.processar(ofertaPublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaSemLancesDisponiveis() {

        final OfertaPublicacao ofertaPublicacao = new OfertaPublicacao();

        ofertaPublicacao.setAlimentoId(2);
        ofertaPublicacao.setEmpresarialId(1);
        ofertaPublicacao.setOfertaId(4);
        ofertaPublicacao.setQuantidadeAlimento(5);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        ofertaPublicacao.setLocalizacao(localizacao);

        ofertaObservador.processar(ofertaPublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    private static Lance obterLanceProxy(OfertaPublicacao ofertaPublicacao, Integer idLance) {

        final Lance lance = new Lance();

        lance.setId(idLance);

        lance.setSituacaoLance(0);

        lance.setAlimentoId(ofertaPublicacao.getAlimentoId());

        lance.setQuantidadeAlimento(ofertaPublicacao.getQuantidadeAlimento());

        return lance;

    }
}
